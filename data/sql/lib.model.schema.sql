
-----------------------------------------------------------------------------
-- contacto
-----------------------------------------------------------------------------

DROP TABLE [contacto];


CREATE TABLE [contacto]
(
	[id] INTEGER  NOT NULL PRIMARY KEY,
	[nombre] VARCHAR(50),
	[apellidos] VARCHAR(50)
);

-----------------------------------------------------------------------------
-- prestamo
-----------------------------------------------------------------------------

DROP TABLE [prestamo];


CREATE TABLE [prestamo]
(
	[id] INTEGER  NOT NULL PRIMARY KEY,
	[fecha] DATETIME,
	[detalles] VARCHAR(255),
	[tipo] INTEGER,
	[valor] INTEGER,
	[fecha_devolucion] DATETIME,
	[nombre] VARCHAR(255),
	[contacto_id] INTEGER  NOT NULL
);

-- SQLite does not support foreign keys; this is just for reference
-- FOREIGN KEY ([contacto_id]) REFERENCES contacto ([id])

-----------------------------------------------------------------------------
-- autor
-----------------------------------------------------------------------------

DROP TABLE [autor];


CREATE TABLE [autor]
(
	[id] INTEGER  NOT NULL PRIMARY KEY,
	[nombre] VARCHAR(255),
	[apellido] VARCHAR(255),
	[detalles] VARCHAR(255)
);

-----------------------------------------------------------------------------
-- fuente
-----------------------------------------------------------------------------

DROP TABLE [fuente];


CREATE TABLE [fuente]
(
	[id] INTEGER  NOT NULL PRIMARY KEY,
	[titulo] VARCHAR(255),
	[tipo] INTEGER,
	[formato] INTEGER,
	[detalles] VARCHAR(255),
	[autor_id] INTEGER  NOT NULL
);

-- SQLite does not support foreign keys; this is just for reference
-- FOREIGN KEY ([autor_id]) REFERENCES autor ([id])

-----------------------------------------------------------------------------
-- frase
-----------------------------------------------------------------------------

DROP TABLE [frase];


CREATE TABLE [frase]
(
	[id] INTEGER  NOT NULL PRIMARY KEY,
	[contenido] VARCHAR(255),
	[autor_id] INTEGER  NOT NULL
);

-- SQLite does not support foreign keys; this is just for reference
-- FOREIGN KEY ([autor_id]) REFERENCES autor ([id])
