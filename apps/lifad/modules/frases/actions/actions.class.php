<?php
/**
 * frases actions.
 *
 * @package    LIFAD
 * @subpackage frases * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class frasesActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeCrear(sfWebRequest $request)
  {
    $respuesta = array();
    $respuesta['success'] = true;
    
    $frase = new frase();
    
          $frase->setContenido($request->getParameter('contenido'));
          $frase->setAutorId($request->getParameter('autor_id'));
        
    $frase->save();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeListar(sfWebRequest $request) {
    $respuesta = array();
    
    $frases = frasePeer::doSelect(new Criteria());
    
    $tuplas = array();
    
    foreach($frases as $frase) {
      $tupla = array();
      
      $tupla['id'] = $frase->getId();
      
            $tupla['contenido'] = $frase->getContenido();      
            $tupla['autor_id'] = $frase->getAutorId();      
            $tuplas[] = $tupla;
    }
    
    $respuesta['success'] = true;
    $respuesta['tuplas'] = $tuplas;
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeEliminar(sfWebRequest $request) {
    $respuesta = array();
    
    $filas = frasePeer::doDelete($request->getParameter('id'));
    
    $respuesta['success'] = ($filas>0);
    
    $respuesta['mensaje'] = array();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeModificar(sfWebRequest $request) {
    $respuesta = array();
    
    $frase = frasePeer::retrieveByPK(intval($request->getParameter('id')));
    
        $frase->setContenido($request->getParameter('contenido'));
        $frase->setAutorId($request->getParameter('autor_id'));
        
    $frase->save();
    
    $respuesta['success'] = true;
    
    $respuesta['mensaje'] = array();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
}
