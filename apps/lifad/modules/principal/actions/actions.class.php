<?php

/**
 * principal actions.
 *
 * @package    LIFAD
 * @subpackage principal
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class principalActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    // $this->forward('default', 'module');
  }
  public function executePoblarMenu() {
	  $arbol = "
	  [
      {
			text:		'Academia',
			children:	[
				{
					id: 	'autores-panel',
					text:	'Autores',
					leaf:	true
				},{
					id: 	'frases-panel',
					text:	'Frases',
					leaf:	true
				},{
					id: 	'fuentes-panel',
					text:	'Biblioteca',
					leaf:	true
				},{
					id: 	'cola-lectura-panel',
					text:	'Cola de lectura',
					leaf:	true
				},{
					id: 	'notas-panel',
					text:	'Notas de lectura',
					leaf:	true
				}
			]
		},{
			text:		'Finanzas',
			children:	[
				{
					id: 	'prestamos-panel',
					text:	'Préstamos',
					leaf:	true
				},{
					id: 	'contactos-panel',
					text:	'Contactos',
					leaf:	true
				}
			]
		},{
      text:		'Negocios',
      children: [
        {
          id: 'inversiones-panel',
          text: 'Inversiones',
          children: [
            {
              id: 'analisis-inversiones-panel',
              text: 'Análisis',
              leaf: true
            },{
              id: 'gestion-inversiones-panel',
              text: 'Gestión',
              leaf: true
            }
          ]
        },{
					id: 	'simulador-bursatil-panel',
					text:	'Simulador Bursátil',
					leaf:	true
				}
      ]
    },{
			text:		'Otros',
			children:	[
        {
					id: 	'generador-mensajes-panel',
					text:	'Generador mensajes',
					leaf:	true
				},{
					id: 	'pinger-panel',
					text:	'Pinger',
					leaf:	true
				},{
					id: 	'software-panel',
					text:	'Software',
					leaf:	true
				}
			]
		}
	  ]
	  ";
	  $this->renderText($arbol);
	  return sfView::NONE;
  }
}
