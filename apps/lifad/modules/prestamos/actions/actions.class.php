<?php
/**
 * prestamos actions.
 *
 * @package    LIFAD
 * @subpackage prestamos * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class prestamosActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeCrear(sfWebRequest $request)
  {
    $respuesta = array();
    $respuesta['success'] = true;
    
    $prestamo = new prestamo();
    
    $prestamo->setContactoId($request->getParameter('contacto_id'));
    
    $prestamo->setTipo($request->getParameter('tipo'));
    $prestamo->setNombre($request->getParameter('nombre'));
    $prestamo->setContactoId($request->getParameter('contacto_id'));
    $prestamo->setValor($request->getParameter('valor'));
    $prestamo->setFecha($request->getParameter('fecha'));
    $prestamo->setFechaDevolucion($request->getParameter('fecha_devolucion'));
    $prestamo->setDetalles($request->getParameter('detalles'));
    
    $prestamo->save();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeListar(sfWebRequest $request) {
    $respuesta = array();
    
    $prestamos = prestamoPeer::doSelect(new Criteria());
    
    $tuplas = array();
    
    foreach($prestamos as $prestamo) {
      $tupla = array();
            $tupla['id'] = $prestamo->getId();
            $tupla['fecha'] = $prestamo->getFecha();      
            $tupla['detalles'] = $prestamo->getDetalles();      
            $tupla['tipo'] = $prestamo->getTipo();      
            $tupla['valor'] = $prestamo->getValor();      
            $tupla['fecha_devolucion'] = $prestamo->getFechaDevolucion();      
            $tupla['nombre'] = $prestamo->getNombre();      
            $tupla['contacto_id'] = $prestamo->getContactoId();      
            $tuplas[] = $tupla;
    }
    
    $respuesta['success'] = true;
    $respuesta['tuplas'] = $tuplas;
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeEliminar(sfWebRequest $request) {
    $respuesta = array();
    
    $filas = prestamoPeer::doDelete($request->getParameter('id'));
    
    $respuesta['success'] = ($filas>0);
    
    $respuesta['mensaje'] = array();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeModificar(sfWebRequest $request) {
    $respuesta = array();
    
    $prestamo = prestamoPeer::retrieveByPK(intval($request->getParameter('id')));
    
        $prestamo->setFecha($request->getParameter('fecha'));
        $prestamo->setDetalles($request->getParameter('detalles'));
        $prestamo->setTipo($request->getParameter('tipo'));
        $prestamo->setValor($request->getParameter('valor'));
        $prestamo->setFechaDevolucion($request->getParameter('fecha_devolucion'));
        $prestamo->setNombre($request->getParameter('nombre'));
        $prestamo->setContactoId($request->getParameter('contacto_id'));
        
    $prestamo->save();
    
    $respuesta['success'] = true;
    
    $respuesta['mensaje'] = array();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
}
