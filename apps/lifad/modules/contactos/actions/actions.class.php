<?php

/**
 * contactos actions.
 *
 * @package    LIFAD
 * @subpackage contactos
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class contactosActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeCrear(sfWebRequest $request)
  {
    $respuesta = array();
    $respuesta['success'] = true;
    
    $contacto = new contacto();
    
    $contacto->setNombre($request->getParameter('nombre'));
    $contacto->setApellidos($request->getParameter('apellidos'));
    
    $contacto->save();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeListar(sfWebRequest $request) {
    $respuesta = array();
    
    $contactos = contactoPeer::doSelect(new Criteria());
    
    $tuplas = array();
    
    foreach($contactos as $contacto) {
      $tupla = array();
      $tupla['id'] = $contacto->getId();
      $tupla['nombre'] = $contacto->getNombre();
      $tupla['apellidos'] = $contacto->getApellidos();
      $tuplas[] = $tupla;
    }
    
    $respuesta['success'] = true;
    $respuesta['tuplas'] = $tuplas;
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeEliminar(sfWebRequest $request) {
    $respuesta = array();
    
    $filas = contactoPeer::doDelete($request->getParameter('id'));
    
    $respuesta['success'] = ($filas>0);
    
    $respuesta['mensaje'] = array();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeModificar(sfWebRequest $request) {
    $respuesta = array();
    
    $contacto = contactoPeer::retrieveByPK(intval($request->getParameter('id')));
    
    $contacto->setNombre($request->getParameter('nombre'));
    $contacto->setApellidos($request->getParameter('apellidos'));
    
    $contacto->save();
    
    $respuesta['success'] = true;
    
    $respuesta['mensaje'] = array();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeListarNombreCompleto(sfWebRequest $request) {
    $respuesta = array();
    
    $contactos = contactoPeer::doSelect(new Criteria());
    
    $tuplas = array();
    
    foreach($contactos as $contacto) {
      $tupla = array();
      $tupla['id'] = $contacto->getId();
      $tupla['nombreCompleto'] = $contacto->getApellidos().' '.$contacto->getNombre();
      $tuplas[] = $tupla;
    }
    
    $respuesta['success'] = true;
    $respuesta['tuplas'] = $tuplas;
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
}
