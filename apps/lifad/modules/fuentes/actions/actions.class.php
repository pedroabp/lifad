<?php
/**
 * fuentes actions.
 *
 * @package    LIFAD
 * @subpackage fuentes * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class fuentesActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeCrear(sfWebRequest $request)
  {
    $this->renderText('Hola');
    
    /*$respuesta = array();
    $respuesta['success'] = true;
    
    $fuente = new fuente();*/
    
    $this->renderText(json_encode($_POST));
//    $this->renderText($request->getParameter('titulo'));
//    $this->renderText($request->getParameter('tipo'));
//    $this->renderText($request->getParameter('formato'));
//    $this->renderText($request->getParameter('autor_id'));
//    $this->renderText($request->getParameter('detalles'));
    
    $this->renderText('Chao');
    
    /*$fuente->setTitulo($request->getParameter('titulo'));
    $fuente->setTipo($request->getParameter('tipo'));
    $fuente->setFormato($request->getParameter('formato'));
    $fuente->setAutorId($request->getParameter('autor_id'));
    $fuente->setDetalles($request->getParameter('detalles'));
    
    $fuente->save();
    
    if($request->getParameter('formato')!='1') {
      $archivos = $request->getFiles();
      $archivo = $archivos['file'];
      $respuesta['success'] = move_uploaded_file($archivo['tmp_name'],$fuente->getRuta());
    }
    
    $this->renderText(json_encode($respuesta));*/
    
    return sfView::NONE;
  }
  public function executeListar(sfWebRequest $request) {
    $respuesta = array();
    
    $fuentes = fuentePeer::doSelect(new Criteria());
    
    $tuplas = array();
    
    foreach($fuentes as $fuente) {
      $tupla = array();
      
      $tupla['id'] = $fuente->getId();
      
            $tupla['titulo'] = $fuente->getTitulo();      
            $tupla['tipo'] = $fuente->getTipo();      
            $tupla['formato'] = $fuente->getFormato();      
            $tupla['autor_id'] = $fuente->getAutorId();      
            $tupla['detalles'] = $fuente->getDetalles();
            $tupla['ruta'] = $fuente->getRuta();
            $tuplas[] = $tupla;
    }
    
    $respuesta['success'] = true;
    $respuesta['tuplas'] = $tuplas;
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeEliminar(sfWebRequest $request) {
    $respuesta = array();
    
    $fuente = fuentePeer::retrieveByPK(intval($request->getParameter('id')));
    
    $filas = fuentePeer::doDelete($request->getParameter('id'));
    
    $respuesta['success'] = ($filas>0);
    
    $respuesta['mensaje'] = array();
    
    unlink($fuente->getRuta());
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeModificar(sfWebRequest $request) {
    $respuesta = array();
    
    $fuente = fuentePeer::retrieveByPK(intval($request->getParameter('id')));
    
        $fuente->setTitulo($request->getParameter('titulo'));
        $fuente->setTipo($request->getParameter('tipo'));
        /*$fuente->setFormato($request->getParameter('formato'));*/
        $fuente->setAutorId($request->getParameter('autor_id'));
        $fuente->setDetalles($request->getParameter('detalles'));
        
    $fuente->save();
    
    $respuesta['success'] = true;
    
    $respuesta['mensaje'] = array();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
}
