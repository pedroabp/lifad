<?php
/**
 * autores actions.
 *
 * @package    LIFAD
 * @subpackage autores * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class autoresActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeCrear(sfWebRequest $request)
  {
    $respuesta = array();
    $respuesta['success'] = true;
    
    $autor = new autor();
    
          $autor->setNombre($request->getParameter('nombre'));
          $autor->setApellido($request->getParameter('apellido'));
          $autor->setDetalles($request->getParameter('detalles'));
        
    $autor->save();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeListar(sfWebRequest $request) {
    $respuesta = array();
    
    $autores = autorPeer::doSelect(new Criteria());
    
    $tuplas = array();
    
    foreach($autores as $autor) {
      $tupla = array();
      
      $tupla['id'] = $autor->getId();
      
            $tupla['nombre'] = $autor->getNombre();      
            $tupla['apellido'] = $autor->getApellido();      
            $tupla['detalles'] = $autor->getDetalles();      
            $tuplas[] = $tupla;
    }
    
    $respuesta['success'] = true;
    $respuesta['tuplas'] = $tuplas;
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeEliminar(sfWebRequest $request) {
    $respuesta = array();
    
    $filas = autorPeer::doDelete($request->getParameter('id'));
    
    $respuesta['success'] = ($filas>0);
    
    $respuesta['mensaje'] = array();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeModificar(sfWebRequest $request) {
    $respuesta = array();
    
    $autor = autorPeer::retrieveByPK(intval($request->getParameter('id')));
    
        $autor->setNombre($request->getParameter('nombre'));
        $autor->setApellido($request->getParameter('apellido'));
        $autor->setDetalles($request->getParameter('detalles'));
        
    $autor->save();
    
    $respuesta['success'] = true;
    
    $respuesta['mensaje'] = array();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeListarNombreCompleto(sfWebRequest $request) {
    $respuesta = array();
    
    $autores = autorPeer::doSelect(new Criteria());
    
    $tuplas = array();
    
    foreach($autores as $autor) {
      $tupla = array();
      $tupla['id'] = $autor->getId();
      $tupla['displayField'] = $autor->getApellido().' '.$autor->getNombre();
      $tuplas[] = $tupla;
    }
    
    $respuesta['success'] = true;
    $respuesta['tuplas'] = $tuplas;
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
}
