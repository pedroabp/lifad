<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php include_http_metas() ?>
<?php include_metas() ?>
<?php include_title() ?>
<link rel="shortcut icon" href="/favicon.ico" />
<script>
    	var url_principal_poblar_menu = '<?php echo url_for('principal/poblarMenu',false)?>';
    	var url_prestamos_listar = '<?php echo url_for('prestamos/listar',false)?>';
    	var url_prestamos_crear = '<?php echo url_for('prestamos/crear',false)?>';
    	var url_prestamos_modificar = '<?php echo url_for('prestamos/modificar',false)?>';
    	var url_prestamos_eliminar = '<?php echo url_for('prestamos/eliminar',false)?>';
    	var url_contactos_listar_nombre_completo = '<?php echo url_for('contactos/listarNombreCompleto',false)?>';
    	var url_contactos_listar = '<?php echo url_for('contactos/listar',false)?>';
    	var url_contactos_crear = '<?php echo url_for('contactos/crear',false)?>';
    	var url_contactos_modificar = '<?php echo url_for('contactos/modificar',false)?>';
    	var url_contactos_eliminar = '<?php echo url_for('contactos/eliminar',false)?>';
    	var url_autores_listar = '<?php echo url_for('autores/listar',false)?>';
    	var url_autores_crear = '<?php echo url_for('autores/crear',false)?>';
    	var url_autores_modificar = '<?php echo url_for('autores/modificar',false)?>';
    	var url_autores_eliminar = '<?php echo url_for('autores/eliminar',false)?>';
    	var url_fuentes_listar = '<?php echo url_for('fuentes/listar',false)?>';
    	var url_fuentes_modificar = '<?php echo url_for('fuentes/modificar',false)?>';
    	var url_autores_listar_nombre_completo = '<?php echo url_for('autores/listarNombreCompleto',false)?>';
    	var url_fuentes_crear = '<?php echo url_for('fuentes/crear',false)?>';
    	var url_fuentes_eliminar = '<?php echo url_for('fuentes/eliminar',false)?>';
    	var url_frases_listar = '<?php echo url_for('frases/listar',false)?>';
      var url_frases_modificar = '<?php echo url_for('frases/modificar',false)?>';
      var url_frases_crear = '<?php echo url_for('frases/crear',false)?>';
      var url_frases_eliminar = '<?php echo url_for('frases/eliminar',false)?>';
    </script>
<?php echo include_javascripts(); ?>
<?php echo include_stylesheets(); ?>
<?php echo javascript_include_tag('prestamos'); ?>
<?php echo javascript_include_tag('contactos'); ?>
<?php echo javascript_include_tag('autores'); ?>
<?php echo javascript_include_tag('fuentes'); ?>
<?php echo javascript_include_tag('frases'); ?>
<?php echo javascript_include_tag('maquetado'); ?>
</head>
<body>
<?php echo $sf_content ?>
</body>
</html>
