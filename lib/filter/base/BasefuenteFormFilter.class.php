<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * fuente filter form base class.
 *
 * @package    LIFAD
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BasefuenteFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'titulo'   => new sfWidgetFormFilterInput(),
      'tipo'     => new sfWidgetFormFilterInput(),
      'formato'  => new sfWidgetFormFilterInput(),
      'detalles' => new sfWidgetFormFilterInput(),
      'autor_id' => new sfWidgetFormPropelChoice(array('model' => 'autor', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'titulo'   => new sfValidatorPass(array('required' => false)),
      'tipo'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'formato'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'detalles' => new sfValidatorPass(array('required' => false)),
      'autor_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'autor', 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('fuente_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'fuente';
  }

  public function getFields()
  {
    return array(
      'id'       => 'Number',
      'titulo'   => 'Text',
      'tipo'     => 'Number',
      'formato'  => 'Number',
      'detalles' => 'Text',
      'autor_id' => 'ForeignKey',
    );
  }
}
