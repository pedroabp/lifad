<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * frase filter form base class.
 *
 * @package    LIFAD
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BasefraseFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'contenido' => new sfWidgetFormFilterInput(),
      'autor_id'  => new sfWidgetFormPropelChoice(array('model' => 'autor', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'contenido' => new sfValidatorPass(array('required' => false)),
      'autor_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'autor', 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('frase_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'frase';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'contenido' => 'Text',
      'autor_id'  => 'ForeignKey',
    );
  }
}
