<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * prestamo filter form base class.
 *
 * @package    LIFAD
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseprestamoFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'fecha'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'detalles'         => new sfWidgetFormFilterInput(),
      'tipo'             => new sfWidgetFormFilterInput(),
      'valor'            => new sfWidgetFormFilterInput(),
      'fecha_devolucion' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'nombre'           => new sfWidgetFormFilterInput(),
      'contacto_id'      => new sfWidgetFormPropelChoice(array('model' => 'contacto', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'fecha'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'detalles'         => new sfValidatorPass(array('required' => false)),
      'tipo'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'valor'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'fecha_devolucion' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'nombre'           => new sfValidatorPass(array('required' => false)),
      'contacto_id'      => new sfValidatorPropelChoice(array('required' => false, 'model' => 'contacto', 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('prestamo_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'prestamo';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'fecha'            => 'Date',
      'detalles'         => 'Text',
      'tipo'             => 'Number',
      'valor'            => 'Number',
      'fecha_devolucion' => 'Date',
      'nombre'           => 'Text',
      'contacto_id'      => 'ForeignKey',
    );
  }
}
