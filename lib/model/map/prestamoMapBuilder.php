<?php


/**
 * This class adds structure of 'prestamo' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * Tue Jan 12 19:55:34 2010
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class prestamoMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.prestamoMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(prestamoPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(prestamoPeer::TABLE_NAME);
		$tMap->setPhpName('prestamo');
		$tMap->setClassname('prestamo');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'INTEGER', true, null);

		$tMap->addColumn('FECHA', 'Fecha', 'DATE', false, null);

		$tMap->addColumn('DETALLES', 'Detalles', 'VARCHAR', false, 255);

		$tMap->addColumn('TIPO', 'Tipo', 'INTEGER', false, null);

		$tMap->addColumn('VALOR', 'Valor', 'INTEGER', false, null);

		$tMap->addColumn('FECHA_DEVOLUCION', 'FechaDevolucion', 'DATE', false, null);

		$tMap->addColumn('NOMBRE', 'Nombre', 'VARCHAR', false, 255);

		$tMap->addForeignKey('CONTACTO_ID', 'ContactoId', 'INTEGER', 'contacto', 'ID', true, null);

	} // doBuild()

} // prestamoMapBuilder
