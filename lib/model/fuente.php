<?php

class fuente extends Basefuente
{
  public function getExtension() {
    $formato = $this->getFormato();
    switch($formato) {
    case '0':
      return 'chm';
      break;
    case '2':
      return 'pdf';
      break;
    case '3':
      return 'zip';
      break;
    }
  }
  public function getRuta() {
    if($this->getFormato()=='1') {
      return '';
    }
    else {
      return 'uploads/fuentes/'.$this->getId().'.'.$this->getExtension();
    }
  }
}
