<?php

/**
 * autor form base class.
 *
 * @package    LIFAD
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseautorForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'       => new sfWidgetFormInputHidden(),
      'nombre'   => new sfWidgetFormInput(),
      'apellido' => new sfWidgetFormInput(),
      'detalles' => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'id'       => new sfValidatorPropelChoice(array('model' => 'autor', 'column' => 'id', 'required' => false)),
      'nombre'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'apellido' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'detalles' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('autor[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'autor';
  }


}
