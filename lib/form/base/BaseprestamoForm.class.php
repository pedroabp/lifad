<?php

/**
 * prestamo form base class.
 *
 * @package    LIFAD
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BaseprestamoForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'fecha'            => new sfWidgetFormDate(),
      'detalles'         => new sfWidgetFormInput(),
      'tipo'             => new sfWidgetFormInput(),
      'valor'            => new sfWidgetFormInput(),
      'fecha_devolucion' => new sfWidgetFormDate(),
      'nombre'           => new sfWidgetFormInput(),
      'contacto_id'      => new sfWidgetFormPropelChoice(array('model' => 'contacto', 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorPropelChoice(array('model' => 'prestamo', 'column' => 'id', 'required' => false)),
      'fecha'            => new sfValidatorDate(array('required' => false)),
      'detalles'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'tipo'             => new sfValidatorInteger(array('required' => false)),
      'valor'            => new sfValidatorInteger(array('required' => false)),
      'fecha_devolucion' => new sfValidatorDate(array('required' => false)),
      'nombre'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'contacto_id'      => new sfValidatorPropelChoice(array('model' => 'contacto', 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('prestamo[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'prestamo';
  }


}
