<?php

/**
 * frase form base class.
 *
 * @package    LIFAD
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 16976 2009-04-04 12:47:44Z fabien $
 */
class BasefraseForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'contenido' => new sfWidgetFormInput(),
      'autor_id'  => new sfWidgetFormPropelChoice(array('model' => 'autor', 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorPropelChoice(array('model' => 'frase', 'column' => 'id', 'required' => false)),
      'contenido' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'autor_id'  => new sfValidatorPropelChoice(array('model' => 'autor', 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('frase[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'frase';
  }


}
