/* This file is created or modified with 
 * Ext.ux.guid.plugin.GuiDesigner (v2.1.0) 
 */
 
//function obtenerRuta() {
// 	URL = ""+document.location;
//  URL = URL.substr(0,URL.lastIndexOf("/"));
//	return URL.substr(0,URL.lastIndexOf("/")+1);
//}
 
Ext.onReady(function() {

Ext.BLANK_IMAGE_URL = '/images/s.gif';
    
Ext.QuickTips.init();
		
var inicio = {
	xtype: 'panel',
	contentEl: 'inicio'
}
		
var cargando_panel = {
  layout: 'fit',
  id: 'cargando-panel',
  title: "Cargando...",
  preparar: function(callback) {
    callback();
  },
  contentEl: 'cargando'
};

Ext.get("imagen_cargando").center();

var contenedor = {
	layout: 'card',
	id: 'contenedor',
	region: 'center',
	activeItem: 0,
	items: [
		inicio,
    cargando_panel,
		prestamos_panel,
		contactos_panel,
    autores_panel,
    fuentes_panel,
    frases_panel
	]
};
		
var treeLoader = new Ext.tree.TreeLoader({
	dataUrl: url_principal_poblar_menu
});

var rootNode = new Ext.tree.AsyncTreeNode();

var tree = new Ext.tree.TreePanel({
	loader: treeLoader,
	root: rootNode,
	rootVisible: false,
	listeners: {
		click: function(nodo, evento) {
			layout = Ext.getCmp('contenedor').layout;
			if(nodo.isLeaf()) {
        if(nodo.id!=layout.activeItem.id) {
          callback = function() {
            layout.setActiveItem(nodo.id);
          }
          layout.setActiveItem('cargando-panel');
          panel = Ext.getCmp(nodo.id);
          panel.preparar(callback);
        }
			}
      else if(nodo.isExpanded()) {
        nodo.collapse();
      }
      else {
        nodo.expand();
      }
		}
	}
});
	
var viewport = new Ext.Viewport({
		layout: "border",
		renderTo: Ext.getBody(),
		items: [
	 	contenedor,
		{
			region: "north",
			title: "LIFAD",
			height: 20
		},    {
			region: "west",
			title: "Men&uacute;",
			width: 200,
			collapsible: true,
			items: [
		 		tree
			],
			split: false
		}]
});
});
