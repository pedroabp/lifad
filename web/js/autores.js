var autores_ds = new Ext.data.Store({
    url:  url_autores_listar,
    reader: new Ext.data.JsonReader({
        root: 'tuplas',
        id: 'id'
    },
    new Ext.data.Record.create([
      { name: 'id', type: 'int', mapping: 'id' },
              { name: 'nombre', type: 'string', mapping: 'nombre' },        
              { name: 'apellido', type: 'string', mapping: 'apellido' },        
              { name: 'detalles', type: 'string', mapping: 'detalles' }        
              ])
    )
});

var autores_cm = new Ext.grid.ColumnModel([
	{header: 'Id', width: 20, sortable: true, dataIndex: 'id'},
    	{header: 'Nombre', width: 120, sortable: true, dataIndex: 'nombre'},  
    	{header: 'Apellido', width: 120, sortable: true, dataIndex: 'apellido'},  
    ]);

function crearAutor() {
  formulario = Ext.getCmp('autores-form').getForm();
  formulario.submit({
      url: url_autores_crear,
      success: function() {
        autores_ds.load();
        modoCrearAutor();
      },
      failure: function() {
        Ext.Msg.alert('Error','No se pudo crear el autor');
      }
  });
}

function modificarAutor() {
  grilla = Ext.getCmp('autores-grid');
  sm = grilla.getSelectionModel();
  if(sm.hasSelection()) {
    tupla = sm.getSelected();
    id_autor = tupla.data.id;
    formulario = Ext.getCmp('autores-form').getForm();
    formulario.submit({
        url: url_autores_modificar,
        success: function() {
          autores_ds.load();
          modoCrearAutor();
        },
        failure: function() {
          Ext.Msg.alert('Error','No se pudo modificar el autor');
        },
        params: {
          id: id_autor        }
    });
  }
  else {
    Ext.Msg.alert('Error', 'Debe seleccionar un autor');
  }
}

function eliminarAutor() {
  grilla = Ext.getCmp('autores-grid');
  sm = grilla.getSelectionModel();
  if(sm.hasSelection()) {
    tupla = sm.getSelected();
    id_autor = tupla.data.id;
    accionEliminar = new Ext.data.Store({
        url:  url_autores_eliminar,
        reader: new Ext.data.JsonReader({
            root: 'mensaje'
        },
        new Ext.data.Record.create([
          { name: 'titulo', type: 'string', mapping: 'titulo' },
          { name: 'cuerpo', type: 'string', mapping: 'cuerpo' }
          ])
        )
    });
    accionEliminar.load({
        callback: function(record,options,success) {
          if(success) {
            autores_ds.load();
            modoCrearAutor();
          }
        },
        params: {
          id: id_autor        }
    });
  }
  else {
    Ext.Msg.alert('Error', 'Debe seleccionar un autor');
  }
}

function modoCrearAutor() {
  boton_crear = Ext.getCmp('boton-crear-autor');
  boton_crear.setText('Crear');
  boton_crear.setHandler(crearAutor);
  Ext.getCmp('boton-cancelar-autor').disable();
  Ext.getCmp('boton-eliminar-autor').disable();
  formulario = Ext.getCmp('autores-form').getForm();
  formulario.reset();
  grilla = Ext.getCmp('autores-grid');
  sm = grilla.getSelectionModel();
  sm.clearSelections();
}

function modoModificarAutor(registro) {
  boton_crear = Ext.getCmp('boton-crear-autor');
  boton_crear.setText('Modificar');
  boton_crear.setHandler(modificarAutor);
  Ext.getCmp('boton-cancelar-autor').enable();
  Ext.getCmp('boton-eliminar-autor').enable();
  formulario = Ext.getCmp('autores-form').getForm();
  formulario.loadRecord(registro);
}

var autores_grid = {
  xtype: "grid",
  id: 'autores-grid',
  frame: true,
  ds: autores_ds,
  cm: autores_cm,
  border: true,
  sm: new Ext.grid.RowSelectionModel({
      singleSelect: true,
      listeners: {
        rowselect: {
          fn: function(sm, index, registro) {
            modoModificarAutor(registro);
          }
        }
      }
  })
}

      
var autores_panel = {
	 layout: "border",
	 id: 'autores-panel',
	 items: [    {
		 region: "center",
     layout: 'fit',
		 title: "Autores",
		 items: [
      autores_grid
     ],
		 frame: true
	 },    {
		 region: "east",
		 title: "Informaci&oacute;n",
		 width: 300,
		 collapsible: true,
		 items: [        {
			 xtype: "form",
       id: 'autores-form',
			 frame: true,
			 items: [
       
                {
           xtype: "textfield",
           name: "nombre",
           fieldLabel: "Nombre",
           allowBlank: false
                               },
                {
           xtype: "textfield",
           name: "apellido",
           fieldLabel: "Apellido",
           allowBlank: false
                               },
                {
           xtype: "textarea",
           name: "detalles",
           fieldLabel: "Detalles",
             width: 160,
             height: 100
                               },
              {
				 xtype: "panel",
				 height: 25
			 },            {
				 layout: "column",
				 items: [                {
					 items: [                    {
						 xtype: "button",
						 text: "Crear",
             id:  'boton-crear-autor',
						 cls: 'x-btn-text-icon',
						 icon: '/images/crear.png'
					 }]
				 },{
					 width:	10,
					 height: 10
				 },                {
					 items: [                    {
						 xtype: "button",
						 text: "Cancelar",
             id:  'boton-cancelar-autor',
						 cls: 'x-btn-text-icon',
						 icon: '/images/cancelar.png',
             handler: modoCrearAutor					 }]
				 },{
					 width:	10,
					 height: 10
				 },                {
					 items: [                    {
						 xtype: "button",
						 text: "Eliminar",
             id:  'boton-eliminar-autor',
						 cls: 'x-btn-text-icon',
						 icon: '/images/eliminar.png',
             handler: eliminarAutor					 }]
				 }]
			 }]
		 }],
		 frame: true
	 }],
	 json: {
		 size: {
			 width: 641,
			 height: 412
		 }
	 },
   preparar: function(callback) {
     autores_ds.load({
         callback: function() {
           modoCrearAutor();
           callback();
         }
     });
   }
 };
