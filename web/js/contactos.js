var contactos_ds = new Ext.data.Store({
    url:  url_contactos_listar,
    reader: new Ext.data.JsonReader({
        root: 'tuplas',
        id: 'id'
    },
    new Ext.data.Record.create([
        { name: 'id', type: 'int', mapping: 'id' },
        { name: 'nombre', type: 'string', mapping: 'nombre' },
        { name: 'apellidos', type: 'string', mapping: 'apellidos' }
      ])
    )
});

var contactos_cm = new Ext.grid.ColumnModel([
	{header: 'Id', width: 20, sortable: true, dataIndex: 'id'},
	{header: 'Nombre', width: 120, sortable: true, dataIndex: 'nombre'},
	{header: 'Apellidos', width: 120, sortable: true, dataIndex: 'apellidos'}
]);

function crearContacto() {
  formulario = Ext.getCmp('contactos-form').getForm();
  formulario.submit({
      url: url_contactos_crear,
      success: function() {
        //Ext.Msg.alert('&Eacute;xito','El contacto fue creado correctamente');
        contactos_ds.load();
        modoCrearContacto();
      },
      failure: function() {
        Ext.Msg.alert('Error','No se pudo crear el contacto');
      }
  });
}

function modificarContacto() {
  grilla = Ext.getCmp('contactos-grid');
  sm = grilla.getSelectionModel();
  if(sm.hasSelection()) {
    tupla = sm.getSelected();
    id_contacto = tupla.data.id;
    formulario = Ext.getCmp('contactos-form').getForm();
    formulario.submit({
        url: url_contactos_modificar,
        success: function() {
          //Ext.Msg.alert('&Eacute;xito','El contacto fue creado correctamente');
          contactos_ds.load();
          modoCrearContacto();
        },
        failure: function() {
          Ext.Msg.alert('Error','No se pudo modificar el contacto');
        },
        params: {
          id: id_contacto
        }
    });
  }
  else {
    Ext.Msg.alert('Error', 'Debe seleccionar un contacto');
  }
}

function eliminarContacto() {
  grilla = Ext.getCmp('contactos-grid');
  sm = grilla.getSelectionModel();
  if(sm.hasSelection()) {
    tupla = sm.getSelected();
    id_contacto = tupla.data.id;
    accionEliminar = new Ext.data.Store({
        url:  url_contactos_eliminar,
        reader: new Ext.data.JsonReader({
            root: 'mensaje'
        },
        new Ext.data.Record.create([
          { name: 'titulo', type: 'string', mapping: 'titulo' },
          { name: 'cuerpo', type: 'string', mapping: 'cuerpo' }
          ])
        )
    });
    accionEliminar.load({
        callback: function(record,options,success) {
          if(success) {
            contactos_ds.load();
            modoCrearContacto();
          }
        },
        params: {
          id: id_contacto
        }
    });
  }
  else {
    Ext.Msg.alert('Error', 'Debe seleccionar un contacto');
  }
}

function modoCrearContacto() {
  boton_crear = Ext.getCmp('boton-crear-contacto');
  boton_crear.setText('Crear');
  boton_crear.setHandler(crearContacto);
  Ext.getCmp('boton-cancelar-contacto').disable();
  Ext.getCmp('boton-eliminar-contacto').disable();
  formulario = Ext.getCmp('contactos-form').getForm();
  formulario.reset();
  grilla = Ext.getCmp('contactos-grid');
  sm = grilla.getSelectionModel();
  sm.clearSelections();
}

function modoModificar(registro) {
  boton_crear = Ext.getCmp('boton-crear-contacto');
  boton_crear.setText('Modificar');
  boton_crear.setHandler(modificarContacto);
  Ext.getCmp('boton-cancelar-contacto').enable();
  Ext.getCmp('boton-eliminar-contacto').enable();
  formulario = Ext.getCmp('contactos-form').getForm();
  formulario.loadRecord(registro);
}

var contactos_grid = {
  xtype: "grid",
  id: 'contactos-grid',
  frame: true,
  ds: contactos_ds,
  cm: contactos_cm,
  border: true,
  sm: new Ext.grid.RowSelectionModel({
      singleSelect: true,
      listeners: {
        rowselect: {
          fn: function(sm, index, registro) {
            modoModificar(registro);
          }
        }
      }
  })
}

var contactos_panel = {
	 layout: "border",
	 id: 'contactos-panel',
	 items: [    {
		 region: "center",
     layout: 'fit',
		 title: "Contactos",
		 items: [
      contactos_grid
     ],
		 frame: true
	 },    {
		 region: "east",
		 title: "Informaci&oacute;n",
		 width: 300,
		 collapsible: true,
		 items: [        {
			 xtype: "form",
       id: 'contactos-form',
			 frame: true,
			 items: [            {
				 xtype: "textfield",
         tabIndex: 0,
				 name: "nombre",
				 fieldLabel: "Nombre",
         allowBlank: false
			 },            {
				 xtype: "textfield",
				 name: "apellidos",
				 fieldLabel: "Apellidos",
         allowBlank: false
			 },            {
				 xtype: "panel",
				 height: 25
			 },            {
				 layout: "column",
				 items: [                {
					 items: [                    {
						 xtype: "button",
						 text: "Crear",
             id:  'boton-crear-contacto',
						 cls: 'x-btn-text-icon',
						 icon: '/images/crear.png'
					 }]
				 },{
					 width:	10,
					 height: 10
				 },                {
					 items: [                    {
						 xtype: "button",
						 text: "Cancelar",
             id:  'boton-cancelar-contacto',
						 cls: 'x-btn-text-icon',
						 icon: '/images/cancelar.png',
             handler: modoCrearContacto
					 }]
				 },{
					 width:	10,
					 height: 10
				 },                {
					 items: [                    {
						 xtype: "button",
						 text: "Eliminar",
             id:  'boton-eliminar-contacto',
						 cls: 'x-btn-text-icon',
						 icon: '/images/eliminar.png',
             handler: eliminarContacto
					 }]
				 }]
			 }]
		 }],
		 frame: true
	 }],
	 json: {
		 size: {
			 width: 641,
			 height: 412
		 }
	 },
   preparar: function(callback) {
     contactos_ds.load({
         callback: function() {
           modoCrearContacto();
           callback();
         }
     });
   }
 };
 
