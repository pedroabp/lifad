var frases_ds = new Ext.data.Store({
    url: url_frases_listar,
    reader: new Ext.data.JsonReader({
        root: 'tuplas',
        id: 'id'
    }, new Ext.data.Record.create([{
        name: 'id',
        type: 'int',
        mapping: 'id'
    }, {
        name: 'contenido',
        type: 'string',
        mapping: 'contenido'
    }, {
        name: 'autor_id',
        type: 'string',
        mapping: 'autor_id'
    }]))
});

function autorFraseRenderer(valor){
    id_autor = valor;
    autores = frases_autor_id_ds.query('id', id_autor, true);
    autor = autores.first();
    return autor.get('displayField');
}

var frases_cm = new Ext.grid.ColumnModel([{
    header: 'Id',
    width: 20,
    sortable: true,
    dataIndex: 'id'
}, {
    header: 'Contenido',
    width: 255,
    sortable: true,
    dataIndex: 'contenido'
}, {
    header: 'Autor',
    width: 200,
    sortable: true,
    dataIndex: 'autor_id',
    renderer: autorFraseRenderer
}]);

function crearFrase(){
    formulario = Ext.getCmp('frases-form').getForm();
    formulario.submit({
        url: url_frases_crear,
        success: function(){
            frases_ds.load();
            modoCrearFrase();
        },
        failure: function(){
            Ext.Msg.alert('Error', 'No se pudo crear el frase');
        }
    });
}

function modificarFrase(){
    grilla = Ext.getCmp('frases-grid');
    sm = grilla.getSelectionModel();
    if (sm.hasSelection()) {
        tupla = sm.getSelected();
        id_frase = tupla.data.id;
        formulario = Ext.getCmp('frases-form').getForm();
        formulario.submit({
            url: url_frases_modificar,
            success: function(){
                frases_ds.load();
                modoCrearFrase();
            },
            failure: function(){
                Ext.Msg.alert('Error', 'No se pudo modificar el frase');
            },
            params: {
                id: id_frase
            }
        });
    }
    else {
        Ext.Msg.alert('Error', 'Debe seleccionar un frase');
    }
}

function eliminarFrase(){
    grilla = Ext.getCmp('frases-grid');
    sm = grilla.getSelectionModel();
    if (sm.hasSelection()) {
        tupla = sm.getSelected();
        id_frase = tupla.data.id;
        accionEliminar = new Ext.data.Store({
            url: url_frases_eliminar,
            reader: new Ext.data.JsonReader({
                root: 'mensaje'
            }, new Ext.data.Record.create([{
                name: 'titulo',
                type: 'string',
                mapping: 'titulo'
            }, {
                name: 'cuerpo',
                type: 'string',
                mapping: 'cuerpo'
            }]))
        });
        accionEliminar.load({
            callback: function(record, options, success){
                if (success) {
                    frases_ds.load();
                    modoCrearFrase();
                }
            },
            params: {
                id: id_frase
            }
        });
    }
    else {
        Ext.Msg.alert('Error', 'Debe seleccionar un frase');
    }
}

function modoCrearFrase(){
    boton_crear = Ext.getCmp('boton-crear-frase');
    boton_crear.setText('Crear');
    boton_crear.setHandler(crearFrase);
    Ext.getCmp('boton-cancelar-frase').disable();
    Ext.getCmp('boton-eliminar-frase').disable();
    formulario = Ext.getCmp('frases-form').getForm();
    formulario.reset();
    grilla = Ext.getCmp('frases-grid');
    sm = grilla.getSelectionModel();
    sm.clearSelections();
}

function modoModificarFrase(registro){
    boton_crear = Ext.getCmp('boton-crear-frase');
    boton_crear.setText('Modificar');
    boton_crear.setHandler(modificarFrase);
    Ext.getCmp('boton-cancelar-frase').enable();
    Ext.getCmp('boton-eliminar-frase').enable();
    formulario = Ext.getCmp('frases-form').getForm();
    formulario.loadRecord(registro);
}

var frases_grid = {
    xtype: "grid",
    id: 'frases-grid',
    frame: true,
    ds: frases_ds,
    cm: frases_cm,
    border: true,
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true,
        listeners: {
            rowselect: {
                fn: function(sm, index, registro){
                    modoModificarFrase(registro);
                }
            }
        }
    })
}

var frases_autor_id_ds = new Ext.data.Store({
    url: url_autores_listar_nombre_completo,
    reader: new Ext.data.JsonReader({
        root: 'tuplas',
        id: 'id'
    }, new Ext.data.Record.create([{
        name: 'id',
        type: 'int',
        mapping: 'id'
    }, {
        name: 'displayField',
        type: 'string',
        mapping: 'displayField'
    }]))
});

var frases_panel = {
    layout: "border",
    id: 'frases-panel',
    items: [{
        region: "center",
        layout: 'fit',
        title: "Frases",
        items: [frases_grid],
        frame: true
    }, {
        region: "east",
        title: "Informaci&oacute;n",
        width: 300,
        collapsible: true,
        items: [{
            xtype: "form",
            id: 'frases-form',
            frame: true,
            items: [{
                xtype: "textarea",
                name: "contenido",
                fieldLabel: "Contenido",
                allowBlank: false,
                width: 160,
                height: 100
            }, {
                xtype: "combo",
                name: "autor_id",
                fieldLabel: "Autor",
                allowBlank: false,
                hiddenName: 'autor_id',
                mode: 'local',
                displayField: 'displayField',
                valueField: 'id',
                store: frases_autor_id_ds,
                minListWidth: 150,
                emptyText: '-- Seleccione un autor --',
                width: 150
            }, {
                xtype: "panel",
                height: 25
            }, {
                layout: "column",
                items: [{
                    items: [{
                        xtype: "button",
                        text: "Crear",
                        id: 'boton-crear-frase',
                        cls: 'x-btn-text-icon',
                        icon: '/images/crear.png'
                    }]
                }, {
                    width: 10,
                    height: 10
                }, {
                    items: [{
                        xtype: "button",
                        text: "Cancelar",
                        id: 'boton-cancelar-frase',
                        cls: 'x-btn-text-icon',
                        icon: '/images/cancelar.png',
                        handler: modoCrearFrase
                    }]
                }, {
                    width: 10,
                    height: 10
                }, {
                    items: [{
                        xtype: "button",
                        text: "Eliminar",
                        id: 'boton-eliminar-frase',
                        cls: 'x-btn-text-icon',
                        icon: '/images/eliminar.png',
                        handler: eliminarFrase
                    }]
                }]
            }]
        }],
        frame: true
    }],
    json: {
        size: {
            width: 641,
            height: 412
        }
    },
    preparar: function(callback){
        frases_autor_id_ds.load({
            callback: function(){
                frases_ds.load({
                    callback: function(){
                        modoCrearFrase();
                        callback();
                    }
                });
            }
        });
    }
};
