var fuentes_ds = new Ext.data.Store({
    url: url_fuentes_listar,
    reader: new Ext.data.JsonReader({
        root: 'tuplas',
        id: 'id'
    }, new Ext.data.Record.create([{
        name: 'id',
        type: 'int',
        mapping: 'id'
    }, {
        name: 'titulo',
        type: 'string',
        mapping: 'titulo'
    }, {
        name: 'tipo',
        type: 'string',
        mapping: 'tipo'
    }, {
        name: 'formato',
        type: 'string',
        mapping: 'formato'
    }, {
        name: 'autor_id',
        type: 'string',
        mapping: 'autor_id'
    }, {
        name: 'detalles',
        type: 'string',
        mapping: 'detalles'
    }, {
        name: 'ruta',
        type: 'string',
        mapping: 'ruta'
    }]))
});

function tipoFuenteRenderer(valor){
    imagen = '';
    switch (valor) {
        case '0':
            imagen = 'blog.png';
            break;
        case '1':
            imagen = 'ciencia.png';
            break;
        case '2':
            imagen = 'periodico.png';
            break;
        case '3':
            imagen = 'libro.png';
            break;
        case '4':
            imagen = 'tutorial.png';
            break;
    }
    return '<img src="/images/' + imagen + '"></img>'
}

function formatoFuenteRenderer(valor){
    imagen = '';
    switch (valor) {
        case '0':
            imagen = 'html_compilado.png';
            break;
        case '1':
            imagen = 'impreso.png';
            break;
        case '2':
            imagen = 'pdf.png';
            break;
        case '3':
            imagen = 'zip.png';
            break;
    }
    return '<img src="/images/' + imagen + '"></img>'
}

function autorFuenteRenderer(valor){
    id_autor = valor;
    autores = fuentes_autor_id_ds.query('id', id_autor, true);
    autor = autores.first();
    return autor.get('displayField');
}

//function obtenerRutaFuente() {
// 	URL = ""+document.location;
//  URL = URL.substr(0,URL.lastIndexOf("/"));
//	return URL.substr(0,URL.lastIndexOf("/")+1);
//}

function urlRenderer(valor){
    if (valor == '') {
        return '----';
    }
    else {
        ruta = '/' + valor;
        return '<a href="' + ruta + '" target="new">' + ruta + '</a>';
        /* return '<a href="file:/'+valor+'" target="new">'+valor+'</a>'; */
    }
}

var fuentes_cm = new Ext.grid.ColumnModel([{
    header: 'Id',
    width: 20,
    sortable: true,
    dataIndex: 'id'
}, {
    header: 'Tipo',
    width: 30,
    sortable: true,
    dataIndex: 'tipo',
    renderer: tipoFuenteRenderer
}, {
    header: 'Titulo',
    width: 240,
    sortable: true,
    dataIndex: 'titulo'
}, {
    header: 'Formato',
    width: 50,
    sortable: true,
    dataIndex: 'formato',
    renderer: formatoFuenteRenderer
}, {
    header: 'Autor',
    width: 200,
    sortable: true,
    dataIndex: 'autor_id',
    renderer: autorFuenteRenderer
}, {
    header: 'URL',
    width: 200,
    sortable: true,
    dataIndex: 'ruta',
    renderer: urlRenderer
}]);

function crearFuente(){
    formulario = Ext.getCmp('fuentes-form').getForm();
    formulario.submit({
        url: url_fuentes_crear,
        success: function(){
            fuentes_ds.load();
            modoCrearFuente();
        },
        failure: function(){
            Ext.Msg.alert('Error', 'No se pudo crear el fuente');
        }
    });
}

function modificarFuente(){
    grilla = Ext.getCmp('fuentes-grid');
    sm = grilla.getSelectionModel();
    if (sm.hasSelection()) {
        tupla = sm.getSelected();
        id_fuente = tupla.data.id;
        formulario = Ext.getCmp('fuentes-form').getForm();
        formulario.submit({
            url: url_fuentes_modificar,
            success: function(){
                fuentes_ds.load();
                modoCrearFuente();
            },
            failure: function(){
                Ext.Msg.alert('Error', 'No se pudo modificar el fuente');
            },
            params: {
                id: id_fuente
            }
        });
    }
    else {
        Ext.Msg.alert('Error', 'Debe seleccionar un fuente');
    }
}

function eliminarFuente(){
    grilla = Ext.getCmp('fuentes-grid');
    sm = grilla.getSelectionModel();
    if (sm.hasSelection()) {
        tupla = sm.getSelected();
        id_fuente = tupla.data.id;
        accionEliminar = new Ext.data.Store({
            url: url_fuentes_eliminar,
            reader: new Ext.data.JsonReader({
                root: 'mensaje'
            }, new Ext.data.Record.create([{
                name: 'titulo',
                type: 'string',
                mapping: 'titulo'
            }, {
                name: 'cuerpo',
                type: 'string',
                mapping: 'cuerpo'
            }]))
        });
        accionEliminar.load({
            callback: function(record, options, success){
                if (success) {
                    fuentes_ds.load();
                    modoCrearFuente();
                }
            },
            params: {
                id: id_fuente
            }
        });
    }
    else {
        Ext.Msg.alert('Error', 'Debe seleccionar un fuente');
    }
}

function modoCrearFuente(){
    boton_crear = Ext.getCmp('boton-crear-fuente');
    boton_crear.setText('Crear');
    boton_crear.setHandler(crearFuente);
    Ext.getCmp('boton-cancelar-fuente').disable();
    Ext.getCmp('boton-eliminar-fuente').disable();
    formulario = Ext.getCmp('fuentes-form').getForm();
    formulario.reset();
    grilla = Ext.getCmp('fuentes-grid');
    sm = grilla.getSelectionModel();
    sm.clearSelections();
    
    fuente_file.enable();
    fuente_formato_combo.enable();
}

function modoModificarFuente(registro){
    boton_crear = Ext.getCmp('boton-crear-fuente');
    boton_crear.setText('Modificar');
    boton_crear.setHandler(modificarFuente);
    Ext.getCmp('boton-cancelar-fuente').enable();
    Ext.getCmp('boton-eliminar-fuente').enable();
    formulario = Ext.getCmp('fuentes-form').getForm();
    formulario.loadRecord(registro);
    
    fuente_file.disable();
    fuente_formato_combo.disable();
}

var fuentes_grid = {
    xtype: "grid",
    id: 'fuentes-grid',
    frame: true,
    ds: fuentes_ds,
    cm: fuentes_cm,
    border: true,
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true,
        listeners: {
            rowselect: {
                fn: function(sm, index, registro){
                    modoModificarFuente(registro);
                }
            }
        }
    })
}

var fuentes_tipo_ds = new Ext.data.SimpleStore({
    fields: ['id', 'displayField'],
    data: [['0', 'Artículo blog'], ['1', 'Artículo científico'], ['2', 'Artículo periodístico'], ['3', 'Libro'], ['4', 'Tutorial']]
});

var fuentes_formato_ds = new Ext.data.SimpleStore({
    fields: ['id', 'displayField'],
    data: [['0', 'HTML compilado'], ['1', 'Impreso'], ['2', 'PDF'], ['3', 'ZIP']]
});

var fuentes_autor_id_ds = new Ext.data.Store({
    url: url_autores_listar_nombre_completo,
    reader: new Ext.data.JsonReader({
        root: 'tuplas',
        id: 'id'
    }, new Ext.data.Record.create([{
        name: 'id',
        type: 'int',
        mapping: 'id'
    }, {
        name: 'displayField',
        type: 'string',
        mapping: 'displayField'
    }]))
});

var fuente_file = new Ext.form.TextField({
    xtype: 'textfield',
    inputType: 'file',
    name: 'file',
    hideLabel: true,
    width: '95%',
    allowBlank: false
});

var fuente_formato_combo = new Ext.form.ComboBox({
    xtype: "combo",
    name: "formato",
    fieldLabel: "Formato",
    allowBlank: false,
    hiddenName: 'formato',
    mode: 'local',
    displayField: 'displayField',
    valueField: 'id',
    store: fuentes_formato_ds,
    minListWidth: 150,
    emptyText: '-- Seleccione un formato --',
    width: 150,
    editable: false,
    triggerAction: 'all',
    listeners: {
        select: function(combo, registro, nuevoValor){
            if (nuevoValor == '1') {
                fuente_file.disable();
            }
            else {
                fuente_file.enable();
            }
        }
    }
});

var fuentes_panel = {
    layout: "border",
    id: 'fuentes-panel',
    items: [{
        region: "center",
        layout: 'fit',
        title: "Fuentes",
        items: [fuentes_grid],
        frame: true
    }, {
        region: "east",
        title: "Información",
        width: 300,
        collapsible: true,
        items: [{
            xtype: "form",
            id: 'fuentes-form',
            fileUpload: true,
            frame: true,
            items: [{
                xtype: "textfield",
                name: "titulo",
                fieldLabel: "Titulo",
                allowBlank: false
            }, {
                xtype: "combo",
                name: "tipo",
                fieldLabel: "Tipo",
                allowBlank: false,
                hiddenName: 'tipo',
                mode: 'local',
                displayField: 'displayField',
                valueField: 'id',
                store: fuentes_tipo_ds,
                minListWidth: 150,
                emptyText: '-- Seleccione un tipo --',
                width: 150,
                editable: false,
                triggerAction: 'all'
            }, fuente_formato_combo, {
                xtype: "combo",
                name: "autor_id",
                fieldLabel: "Autor",
                allowBlank: false,
                hiddenName: 'autor_id',
                mode: 'local',
                displayField: 'displayField',
                valueField: 'id',
                store: fuentes_autor_id_ds,
                minListWidth: 150,
                emptyText: '-- Seleccione un autor --',
                width: 150
            }, {
                xtype: "textarea",
                name: "detalles",
                fieldLabel: "Detalles",
                width: 160,
                height: 100
            }, {
                xtype: 'fieldset',
                title: 'Archivo',
                autoHeight: true,
                items: [{
                    xtype: 'hidden',
                    name: 'MAX_FILE_SIZE',
                    value: '1000000000'
                }, fuente_file]
            }, {
                xtype: "panel",
                height: 25
            }, {
                layout: "column",
                items: [{
                    items: [{
                        xtype: "button",
                        text: "Crear",
                        id: 'boton-crear-fuente',
                        cls: 'x-btn-text-icon',
                        icon: '/images/crear.png'
                    }]
                }, {
                    width: 10,
                    height: 10
                }, {
                    items: [{
                        xtype: "button",
                        text: "Cancelar",
                        id: 'boton-cancelar-fuente',
                        cls: 'x-btn-text-icon',
                        icon: '/images/cancelar.png',
                        handler: modoCrearFuente
                    }]
                }, {
                    width: 10,
                    height: 10
                }, {
                    items: [{
                        xtype: "button",
                        text: "Eliminar",
                        id: 'boton-eliminar-fuente',
                        cls: 'x-btn-text-icon',
                        icon: '/images/eliminar.png',
                        handler: eliminarFuente
                    }]
                }]
            }]
        }],
        frame: true
    }],
    json: {
        size: {
            width: 641,
            height: 412
        }
    },
    preparar: function(callback){
        fuentes_autor_id_ds.load({
            callback: function(){
                fuentes_ds.load({
                    callback: function(){
                        modoCrearFuente();
                        callback();
                    }
                });
            }
        });
    }
};
