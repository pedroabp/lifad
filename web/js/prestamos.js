var prestamos_ds = new Ext.data.Store({
    url:  url_prestamos_listar,
    reader: new Ext.data.JsonReader({
        root: 'tuplas',
        id: 'id'
    },
    new Ext.data.Record.create([
      { name: 'id', type: 'int', mapping: 'id' },
              { name: 'fecha', type: 'date', mapping: 'fecha', dateFormat: 'Y-m-d' },        
              { name: 'detalles', type: 'string', mapping: 'detalles' },        
              { name: 'tipo', type: 'string', mapping: 'tipo' },        
              { name: 'valor', type: 'int', mapping: 'valor' },        
              { name: 'fecha_devolucion', type: 'date', mapping: 'fecha_devolucion', dateFormat: 'Y-m-d'  },        
              { name: 'nombre', type: 'string', mapping: 'nombre' },        
              { name: 'contacto_id', type: 'string', mapping: 'contacto_id' }        
              ])
    )
});

var contactos_prestamos_ds = new Ext.data.Store({
    url:  url_contactos_listar_nombre_completo,
    reader: new Ext.data.JsonReader({
        root: 'tuplas',
        id: 'id'
    },
    new Ext.data.Record.create([
        { name: 'id', type: 'int', mapping: 'id' },
        { name: 'nombreCompleto', type: 'string', mapping: 'nombreCompleto' }
      ])
    )
});

function tipoPrestamoRenderer(valor) {
  if(valor=='0') {
    return '<img src="/images/dinero.png"></img>'
  }
  else {
    return '<img src="/images/objeto.png"></img>'
  }
}

function contactoPrestamoRenderer(valor) {
  contactos = contactos_prestamos_ds.query('id',valor);
  contacto = contactos.first();
  return contacto.get('nombreCompleto');
}

function nombrePrestamoRenderer(valor, metadatos, registro) {
  if(registro.get('tipo')=='0') {
    return '----';
  }
  else {
    return registro.get('nombre');
  }
}

function valorPrestamoRenderer(valor) {
  valorEntero = parseInt(valor);
  if(valorEntero<0) {
    return '<div style="color: red;">$ '+(valor*-1)+'</div>';
  }
  return '$ '+valor;
}

var prestamos_cm = new Ext.grid.ColumnModel([
	{header: 'Id', width: 20, sortable: true, dataIndex: 'id'},
  {header: 'Tipo', width: 30, sortable: true, dataIndex: 'tipo', renderer: tipoPrestamoRenderer},
  {header: 'Contacto', width: 200, sortable: true, dataIndex: 'contacto_id', renderer: contactoPrestamoRenderer},
  {header: 'Nombre', width: 120, sortable: true, dataIndex: 'nombre', renderer: nombrePrestamoRenderer},
  {header: 'Fecha', width: 70, sortable: true, dataIndex: 'fecha', renderer: Ext.util.Format.dateRenderer('d-m-Y')},    
  {header: 'Valor', width: 100, sortable: true, dataIndex: 'valor', renderer: valorPrestamoRenderer, align: 'right'}
  ]);

function crearPrestamo() {
  formulario = Ext.getCmp('prestamos-form').getForm();
  formulario.submit({
      url: url_prestamos_crear,
      success: function() {
        modoCrearPrestamo();
        prestamos_ds.load();
      },
      failure: function() {
        Ext.Msg.alert('Error','No se pudo crear el prestamo');
      }
  });
}

function modificarPrestamo() {
  grilla = Ext.getCmp('prestamos-grid');
  sm = grilla.getSelectionModel();
  if(sm.hasSelection()) {
    tupla = sm.getSelected();
    id_prestamo = tupla.data.id;
    formulario = Ext.getCmp('prestamos-form').getForm();
    formulario.submit({
        url: url_prestamos_modificar,
        success: function() {
          prestamos_ds.load();
          modoCrearPrestamo();
        },
        failure: function() {
          Ext.Msg.alert('Error','No se pudo modificar el prestamo');
        },
        params: {
          id: id_prestamo        }
    });
  }
  else {
    Ext.Msg.alert('Error', 'Debe seleccionar un prestamo');
  }
}

function eliminarPrestamo() {
  grilla = Ext.getCmp('prestamos-grid');
  sm = grilla.getSelectionModel();
  if(sm.hasSelection()) {
    tupla = sm.getSelected();
    id_prestamo = tupla.data.id;
    accionEliminar = new Ext.data.Store({
        url:  url_prestamos_eliminar,
        reader: new Ext.data.JsonReader({
            root: 'mensaje'
        },
        new Ext.data.Record.create([
          { name: 'titulo', type: 'string', mapping: 'titulo' },
          { name: 'cuerpo', type: 'string', mapping: 'cuerpo' }
          ])
        )
    });
    accionEliminar.load({
        callback: function(record,options,success) {
          if(success) {
            prestamos_ds.load();
            modoCrearPrestamo();
          }
        },
        params: {
          id: id_prestamo        }
    });
  }
  else {
    Ext.Msg.alert('Error', 'Debe seleccionar un prestamo');
  }
}

function modoCrearPrestamo() {
  boton_crear = Ext.getCmp('boton-crear-prestamo');
  boton_crear.setText('Crear');
  boton_crear.setHandler(crearPrestamo);
  Ext.getCmp('boton-cancelar-prestamo').disable();
  Ext.getCmp('boton-eliminar-prestamo').disable();
  formulario = Ext.getCmp('prestamos-form').getForm();
  formulario.reset();
  grilla = Ext.getCmp('prestamos-grid');
  sm = grilla.getSelectionModel();
  sm.clearSelections();
  
  modoPrestamoObjeto();
}

function modoModificarPrestamo(registro) {
  boton_crear = Ext.getCmp('boton-crear-prestamo');
  boton_crear.setText('Modificar');
  boton_crear.setHandler(modificarPrestamo);
  Ext.getCmp('boton-cancelar-prestamo').enable();
  Ext.getCmp('boton-eliminar-prestamo').enable();
  formulario = Ext.getCmp('prestamos-form').getForm();
  formulario.loadRecord(registro);
  
  actualizarModoTipoPrestamo();
}

var prestamos_grid = {
  xtype: "grid",
  id: 'prestamos-grid',
  frame: true,
  ds: prestamos_ds,
  cm: prestamos_cm,
  border: true,
  sm: new Ext.grid.RowSelectionModel({
      singleSelect: true,
      listeners: {
        rowselect: {
          fn: function(sm, index, registro) {
            modoModificarPrestamo(registro);
          }
        }
      }
  })
}

var prestamos_tipos_ss = new Ext.data.SimpleStore({
    fields: ['id', 'tipo'],
    data: [['0','Dinero'],['1','Objeto']]
});

function modoPrestamoDinero() {
  Ext.getCmp('nombre-prestamo').disable();
}

function modoPrestamoObjeto() {
  Ext.getCmp('nombre-prestamo').enable();
}

function actualizarModoTipoPrestamo() {
  valor = Ext.getCmp('tipo-prestamo').getValue();
  if(valor=='0') {
    modoPrestamoDinero();
  }
  else {
    modoPrestamoObjeto();
  }
}

var prestamos_panel = {
	 layout: "border",
	 id: 'prestamos-panel',
	 items: [    {
		 region: "center",
     layout: 'fit',
		 title: "Préstamos",
		 items: [
      prestamos_grid
     ],
		 frame: true
	 },    {
		 region: "east",
		 title: "Información",
		 width: 350,
		 collapsible: true,
		 items: [        {
			 xtype: "form",
       id: 'prestamos-form',
			 frame: true,
			 items: [
       {
           xtype: "combo",
           tabIndex: 0,
           name: "tipo-visible",
           hiddenName: "tipo",
           fieldLabel: "Tipo",
           allowBlank: false,
           store: prestamos_tipos_ss,
           mode: 'local',
           displayField: 'tipo',
           valueField: 'id',
           editable: false,
           triggerAction: 'all',
           emptyText: '-- Selecciona un tipo --',
           minListWidth: 150,
           id: 'tipo-prestamo',
           listeners: {
             select: function(combo, registro, indice) {
               if(indice==0) {
                 modoPrestamoDinero();
               }
               else {
                 modoPrestamoObjeto();
               }
             }
           }
                    },
                    {
           xtype: "textfield",
           name: "nombre",
           id: 'nombre-prestamo',
           fieldLabel: "Nombre",
           allowBlank: false
                    },
                    {
           xtype: "combo",
           name: "contacto_id-visible",
           hiddenName: 'contacto_id',
           fieldLabel: "Contacto",
           allowBlank: false,
           store: contactos_prestamos_ds,
           mode: 'local',
           displayField: 'nombreCompleto',
           valueField:  'id',
           minListWidth: 200,
           emptyText: '-- Seleccione un contacto --',
           width: 200
                    },
                    {
           xtype: "numberfield",
           name: "valor",
           fieldLabel: "Valor ($)",
           allowBlank: false
                    },
                {
           xtype: "datefield",
           name: "fecha",
           fieldLabel: "Fecha",
           allowBlank: false,
           format: 'd-m-Y'
                    },
                    {
           xtype: "datefield",
           name: "fecha_devolucion",
           fieldLabel: "Fecha Devolucion",
           format: 'd-m-Y'
                    },
                {
           xtype: "textarea",
           name: "detalles",
           fieldLabel: "Detalles",
             width: 160,
             height: 100
                    },
              {
				 xtype: "panel",
				 height: 25
			 },            {
				 layout: "column",
				 items: [                {
					 items: [                    {
						 xtype: "button",
						 text: "Crear",
             id:  'boton-crear-prestamo',
						 cls: 'x-btn-text-icon',
						 icon: '/images/crear.png'
					 }]
				 },{
					 width:	10,
					 height: 10
				 },                {
					 items: [                    {
						 xtype: "button",
						 text: "Cancelar",
             id:  'boton-cancelar-prestamo',
						 cls: 'x-btn-text-icon',
						 icon: '/images/cancelar.png',
             handler: modoCrearPrestamo					 }]
				 },{
					 width:	10,
					 height: 10
				 },                {
					 items: [                    {
						 xtype: "button",
						 text: "Eliminar",
             id:  'boton-eliminar-prestamo',
						 cls: 'x-btn-text-icon',
						 icon: '/images/eliminar.png',
             handler: eliminarPrestamo					 }]
				 }]
			 }]
		 }],
		 frame: true
	 }],
	 json: {
		 size: {
			 width: 641,
			 height: 412
		 }
	 },
   preparar: function(callback) {
     contactos_prestamos_ds.load({
         callback: function() {
           prestamos_ds.load({
               callback: function() {
                 modoCrearPrestamo();
                 callback();
               }
           });
         }
     });
   }
 };
