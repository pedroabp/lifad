<?php 

  $campos = array();
  
  /*
  
  $campos[] = array('nombre' => 'fecha', 'tipo' => 2, 'encabezado' => 'Fecha');
  $campos[] = array('nombre' => 'detalles', 'tipo' => 3, 'encabezado' => 'Detalles', 'visible' => false);
  $campos[] = array('nombre' => 'tipo', 'tipo' => 4, 'encabezado' => 'Tipo');
  $campos[] = array('nombre' => 'valor', 'tipo' => 1, 'encabezado' => 'Valor');
  $campos[] = array('nombre' => 'fecha_devolucion', 'tipo' => 2, 'encabezado' => 'Fecha Devolucion');
  $campos[] = array('nombre' => 'nombre', 'tipo' => 0, 'encabezado' => 'Nombre');
  $campos[] = array('nombre' => 'contacto_id', 'tipo' => 4, 'encabezado' => 'Contacto');
  
  */
  
  $nombreSingular = 'frase';
  $nombrePlural = 'frases';
  
  $campos[] = array('nombre' => 'contenido', 'tipo' => 3, 'encabezado' => 'Contenido');
  $campos[] = array('nombre' => 'autor_id', 'tipo' => 4, 'encabezado' => 'Autor');
  
  /*
  Tipos:
  
  0 -> String
  1 -> Integer
  2 -> Date
  3 -> String Largo
  4 -> Opciones
  
  */
?>
<?php 
  function intToTipo($entero) {
    switch($entero) {
    case 0:
      return 'string';
    case 1:
      return 'integer';
    case 2:
      return 'date';
    }
  }
  
  function intToXTipo($entero) {
    switch($entero) {
    case 0:
      return 'textfield';
    case 1:
      return 'numberfield';
    case 2:
      return 'datefield';
    case 3:
      return 'textarea';
    case 4:
      return 'combo';
    }
  }
  
  function to_camel_case($str, $capitalise_first_char = false) {
    if($capitalise_first_char) {
      $str[0] = strtoupper($str[0]);
    }
    return preg_replace_callback('/_([a-z])/', create_function('$c', 'return strtoupper($c[1]);'), $str);
  }
?>
<?php echo '<?php' ?>

/**
 * <?php echo $nombrePlural ?> actions.
 *
 * @package    LIFAD
 * @subpackage <?php echo $nombrePlural ?>
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class <?php echo $nombrePlural ?>Actions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeCrear(sfWebRequest $request)
  {
    $respuesta = array();
    $respuesta['success'] = true;
    
    $<?php echo $nombreSingular ?> = new <?php echo $nombreSingular ?>();
    
    <?php $numeroCampos = count($campos); for($i=0;$i<$numeroCampos;$i++) { $campo = $campos[$i]; ?>
      $<?php echo $nombreSingular ?>->set<?php echo to_camel_case($campo['nombre'],true) ?>($request->getParameter('<?php echo $campo['nombre'] ?>'));
    <?php } ?>
    
    $<?php echo $nombreSingular ?>->save();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeListar(sfWebRequest $request) {
    $respuesta = array();
    
    $<?php echo $nombrePlural ?> = <?php echo $nombreSingular ?>Peer::doSelect(new Criteria());
    
    $tuplas = array();
    
    foreach($<?php echo $nombrePlural ?> as $<?php echo $nombreSingular ?>) {
      $tupla = array();
      
      $tupla['id'] = $<?php echo $nombreSingular ?>->get<?php echo to_camel_case('Id',true) ?>();
      
      <?php for($i=0;$i<$numeroCampos;$i++) { $campo = $campos[$i]; ?>
      $tupla['<?php echo $campo['nombre'] ?>'] = $<?php echo $nombreSingular ?>->get<?php echo to_camel_case($campo['nombre'],true) ?>();      
      <?php } ?>
      $tuplas[] = $tupla;
    }
    
    $respuesta['success'] = true;
    $respuesta['tuplas'] = $tuplas;
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeEliminar(sfWebRequest $request) {
    $respuesta = array();
    
    $filas = <?php echo $nombreSingular ?>Peer::doDelete($request->getParameter('id'));
    
    $respuesta['success'] = ($filas>0);
    
    $respuesta['mensaje'] = array();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
  public function executeModificar(sfWebRequest $request) {
    $respuesta = array();
    
    $<?php echo $nombreSingular ?> = <?php echo $nombreSingular ?>Peer::retrieveByPK(intval($request->getParameter('id')));
    
    <?php for($i=0;$i<$numeroCampos;$i++) { $campo = $campos[$i]; ?>
    $<?php echo $nombreSingular ?>->set<?php echo to_camel_case($campo['nombre'],true) ?>($request->getParameter('<?php echo $campo['nombre'] ?>'));
    <?php } ?>
    
    $<?php echo $nombreSingular ?>->save();
    
    $respuesta['success'] = true;
    
    $respuesta['mensaje'] = array();
    
    $this->renderText(json_encode($respuesta));
    
    return sfView::NONE;
  }
}
