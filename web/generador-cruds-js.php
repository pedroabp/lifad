<?php 

  $campos = array();
  
  /*
  
  $campos[] = array('nombre' => 'fecha', 'tipo' => 2, 'encabezado' => 'Fecha');
  $campos[] = array('nombre' => 'detalles', 'tipo' => 3, 'encabezado' => 'Detalles', 'visible' => false);
  $campos[] = array('nombre' => 'tipo', 'tipo' => 4, 'encabezado' => 'Tipo');
  $campos[] = array('nombre' => 'valor', 'tipo' => 1, 'encabezado' => 'Valor');
  $campos[] = array('nombre' => 'fecha_devolucion', 'tipo' => 2, 'encabezado' => 'Fecha Devolucion');
  $campos[] = array('nombre' => 'nombre', 'tipo' => 0, 'encabezado' => 'Nombre');
  $campos[] = array('nombre' => 'contacto_id', 'tipo' => 4, 'encabezado' => 'Contacto');
  
  */
  
  $nombreSingular = 'frase';
  $nombrePlural = 'frases';
  
  $campos[] = array('nombre' => 'contenido', 'tipo' => 3, 'encabezado' => 'Contenido');
  $campos[] = array('nombre' => 'autor_id', 'tipo' => 4, 'encabezado' => 'Autor');
  
  /*
  Tipos:
  
  0 -> String
  1 -> Integer
  2 -> Date
  3 -> String Largo
  4 -> Opciones
  
  */
?>
<?php 
  function intToTipo($entero) {
    switch($entero) {
    case 0:
      return 'string';
    case 1:
      return 'int';
    case 2:
      return 'date';
    case 3:
      return 'string';
    case 4:
      return 'string';
    }
  }
  
  function intToXTipo($entero) {
    switch($entero) {
    case 0:
      return 'textfield';
    case 1:
      return 'numberfield';
    case 2:
      return 'datefield';
    case 3:
      return 'textarea';
    case 4:
      return 'combo';
    }
  }
  
  function to_camel_case($str, $capitalise_first_char = false) {
    if($capitalise_first_char) {
      $str[0] = strtoupper($str[0]);
    }
    return preg_replace_callback('/_([a-z])/', create_function('$c', 'return strtoupper($c[1]);'), $str);
  }
?>
var <?php echo $nombrePlural ?>_ds = new Ext.data.Store({
    url:  '<?php echo $nombrePlural ?>/listar',
    reader: new Ext.data.JsonReader({
        root: 'tuplas',
        id: 'id'
    },
    new Ext.data.Record.create([
      { name: 'id', type: 'int', mapping: 'id' },
      <?php $numeroCampos = count($campos); for($i=0;$i<$numeroCampos;$i++) { $campo = $campos[$i]; ?>
        { name: '<?php echo $campo['nombre'] ?>', type: '<?php echo intToTipo($campo['tipo']) ?>', mapping: '<?php echo $campo['nombre'] ?>' }<?php if($i!=($numeroCampos-1)) { echo ','; } ?>
        
      <?php } ?>
        ])
    )
});

var <?php echo $nombrePlural ?>_cm = new Ext.grid.ColumnModel([
	{header: 'Id', width: 20, sortable: true, dataIndex: 'id'},
  <?php for($i=0;$i<$numeroCampos;$i++) { $campo = $campos[$i]; ?>
  <?php if(isset($campo['visible']) && !$campo['visible']) { continue; } ?>
	{header: '<?php echo $campo['encabezado'] ?>', width: 120, sortable: true, dataIndex: '<?php echo $campo['nombre'] ?>'}<?php if($i!=($numeroCampos-1)) { echo ','; } ?>
  
  <?php } ?>
]);

function crear<?php echo to_camel_case($nombreSingular,true) ?>() {
  formulario = Ext.getCmp('<?php echo $nombrePlural ?>-form').getForm();
  formulario.submit({
      url: '<?php echo $nombrePlural ?>/crear',
      success: function() {
        <?php echo $nombrePlural ?>_ds.load();
        modoCrear<?php echo to_camel_case($nombreSingular,true) ?>();
      },
      failure: function() {
        Ext.Msg.alert('Error','No se pudo crear el <?php echo $nombreSingular ?>');
      }
  });
}

function modificar<?php echo to_camel_case($nombreSingular,true) ?>() {
  grilla = Ext.getCmp('<?php echo $nombrePlural ?>-grid');
  sm = grilla.getSelectionModel();
  if(sm.hasSelection()) {
    tupla = sm.getSelected();
    id_<?php echo $nombreSingular ?> = tupla.data.id;
    formulario = Ext.getCmp('<?php echo $nombrePlural ?>-form').getForm();
    formulario.submit({
        url: '<?php echo $nombrePlural ?>/modificar',
        success: function() {
          <?php echo $nombrePlural ?>_ds.load();
          modoCrear<?php echo to_camel_case($nombreSingular,true) ?>();
        },
        failure: function() {
          Ext.Msg.alert('Error','No se pudo modificar el <?php echo $nombreSingular ?>');
        },
        params: {
          id: id_<?php echo $nombreSingular ?>
        }
    });
  }
  else {
    Ext.Msg.alert('Error', 'Debe seleccionar un <?php echo $nombreSingular ?>');
  }
}

function eliminar<?php echo to_camel_case($nombreSingular,true) ?>() {
  grilla = Ext.getCmp('<?php echo $nombrePlural ?>-grid');
  sm = grilla.getSelectionModel();
  if(sm.hasSelection()) {
    tupla = sm.getSelected();
    id_<?php echo $nombreSingular ?> = tupla.data.id;
    accionEliminar = new Ext.data.Store({
        url:  '<?php echo $nombrePlural ?>/eliminar',
        reader: new Ext.data.JsonReader({
            root: 'mensaje'
        },
        new Ext.data.Record.create([
          { name: 'titulo', type: 'string', mapping: 'titulo' },
          { name: 'cuerpo', type: 'string', mapping: 'cuerpo' }
          ])
        )
    });
    accionEliminar.load({
        callback: function(record,options,success) {
          if(success) {
            <?php echo $nombrePlural ?>_ds.load();
            modoCrear<?php echo to_camel_case($nombreSingular,true) ?>();
          }
        },
        params: {
          id: id_<?php echo $nombreSingular ?>
        }
    });
  }
  else {
    Ext.Msg.alert('Error', 'Debe seleccionar un <?php echo $nombreSingular ?>');
  }
}

function modoCrear<?php echo to_camel_case($nombreSingular,true) ?>() {
  boton_crear = Ext.getCmp('boton-crear-<?php echo $nombreSingular ?>');
  boton_crear.setText('Crear');
  boton_crear.setHandler(crear<?php echo to_camel_case($nombreSingular,true) ?>);
  Ext.getCmp('boton-cancelar-<?php echo $nombreSingular ?>').disable();
  Ext.getCmp('boton-eliminar-<?php echo $nombreSingular ?>').disable();
  formulario = Ext.getCmp('<?php echo $nombrePlural ?>-form').getForm();
  formulario.reset();
  grilla = Ext.getCmp('<?php echo $nombrePlural ?>-grid');
  sm = grilla.getSelectionModel();
  sm.clearSelections();
}

function modoModificar<?php echo to_camel_case($nombreSingular,true) ?>(registro) {
  boton_crear = Ext.getCmp('boton-crear-<?php echo $nombreSingular ?>');
  boton_crear.setText('Modificar');
  boton_crear.setHandler(modificar<?php echo to_camel_case($nombreSingular,true) ?>);
  Ext.getCmp('boton-cancelar-<?php echo $nombreSingular ?>').enable();
  Ext.getCmp('boton-eliminar-<?php echo $nombreSingular ?>').enable();
  formulario = Ext.getCmp('<?php echo $nombrePlural ?>-form').getForm();
  formulario.loadRecord(registro);
}

var <?php echo $nombrePlural ?>_grid = {
  xtype: "grid",
  id: '<?php echo $nombrePlural ?>-grid',
  frame: true,
  ds: <?php echo $nombrePlural ?>_ds,
  cm: <?php echo $nombrePlural ?>_cm,
  border: true,
  sm: new Ext.grid.RowSelectionModel({
      singleSelect: true,
      listeners: {
        rowselect: {
          fn: function(sm, index, registro) {
            modoModificar<?php echo to_camel_case($nombreSingular,true) ?>(registro);
          }
        }
      }
  })
}

<?php for($i=0;$i<$numeroCampos;$i++) { $campo = $campos[$i]; ?>
  <?php if($campo['tipo']==4) { ?>
    <?php if(!isset($campo['estatico'])) { $campo['estatico'] = true; }?>
    <?php if($campo['estatico']) { ?>
        var <?php echo $nombrePlural ?>_<?php echo $campo['nombre'] ?>_ds = new Ext.data.SimpleStore({
            fields: ['id', 'displayField'],
            data: [['0','Dato 1'],['1','Dato 2']]
        });
    <?php } else { ?>
        var <?php echo $nombrePlural ?>_<?php echo $campo['nombre'] ?>_ds = new Ext.data.Store({
            url:  '',
            reader: new Ext.data.JsonReader({
                root: 'tuplas',
                id: 'id'
            },
            new Ext.data.Record.create([
                { name: 'id', type: 'int', mapping: 'id' },
                { name: 'displayField', type: 'string', mapping: 'displayField' }
              ])
            )
        });
    <?php } ?>
  <?php } ?>
<?php } ?>

var <?php echo $nombrePlural ?>_panel = {
	 layout: "border",
	 id: '<?php echo $nombrePlural ?>-panel',
	 items: [    {
		 region: "center",
     layout: 'fit',
		 title: "<?php echo to_camel_case($nombrePlural,true) ?>",
		 items: [
      <?php echo $nombrePlural ?>_grid
     ],
		 frame: true
	 },    {
		 region: "east",
		 title: "Informaci&oacute;n",
		 width: 300,
		 collapsible: true,
		 items: [        {
			 xtype: "form",
       id: '<?php echo $nombrePlural ?>-form',
			 frame: true,
			 items: [
       
       <?php for($i=0;$i<$numeroCampos;$i++) { $campo = $campos[$i]; ?>
         {
           xtype: "<?php echo intToXTipo($campo['tipo']) ?>",
           name: "<?php echo $campo['nombre'] ?>",
           fieldLabel: "<?php echo $campo['encabezado'] ?>",
           allowBlank: false
           <?php if($campo['tipo']==3) { ?>
             ,
             width: 160,
             height: 100
           <?php } ?>
           <?php if($campo['tipo']==4) { ?>
             ,
             hiddenName: '<?php echo $campo['nombre'] ?>',
             mode: 'local',
             displayField: 'displayField',
             valueField:  'id',
             store: <?php echo $nombrePlural ?>_<?php echo $campo['nombre'] ?>_ds,
             minListWidth: 150,
             emptyText: '-- Seleccione un ??? --',
             width: 150
             <?php if($campo['estatico']) { ?>
               ,
               editable: false,
               triggerAction: 'all'
             <?php } ?>
           <?php } ?>
         },
       <?php } ?>
       {
				 xtype: "panel",
				 height: 25
			 },            {
				 layout: "column",
				 items: [                {
					 items: [                    {
						 xtype: "button",
						 text: "Crear",
             id:  'boton-crear-<?php echo $nombreSingular ?>',
						 cls: 'x-btn-text-icon',
						 icon: '/images/crear.png'
					 }]
				 },{
					 width:	10,
					 height: 10
				 },                {
					 items: [                    {
						 xtype: "button",
						 text: "Cancelar",
             id:  'boton-cancelar-<?php echo $nombreSingular ?>',
						 cls: 'x-btn-text-icon',
						 icon: '/images/cancelar.png',
             handler: modoCrear<?php echo to_camel_case($nombreSingular,true) ?>
					 }]
				 },{
					 width:	10,
					 height: 10
				 },                {
					 items: [                    {
						 xtype: "button",
						 text: "Eliminar",
             id:  'boton-eliminar-<?php echo $nombreSingular ?>',
						 cls: 'x-btn-text-icon',
						 icon: '/images/eliminar.png',
             handler: eliminar<?php echo to_camel_case($nombreSingular,true) ?>
					 }]
				 }]
			 }]
		 }],
		 frame: true
	 }],
	 json: {
		 size: {
			 width: 641,
			 height: 412
		 }
	 },
   preparar: function(callback) {
     <?php echo $nombrePlural ?>_ds.load({
         callback: function() {
           modoCrear<?php echo to_camel_case($nombreSingular,true) ?>();
           callback();
         }
     });
   }
 };
